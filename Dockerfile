FROM  nginx:latest
RUN apt update && apt upgrade -y && apt install git -y && apt autoclean -y && apt autoremove -y
RUN rm -rf /usr/share/nginx/html && git clone -b fix-upside-down https://github.com/doccaz/spice-web-client.git /usr/share/nginx/html && chown -R www-data.www-data /usr/share/nginx/html/
WORKDIR /usr/share/nginx/html/
EXPOSE 80 443
